*** Settings ***
Documentation     A test suite to test the company web site show essential information
...
Library           SeleniumLibrary
Suite Setup       Go to home page
Suite Teardown    Close All Browsers

*** Test Cases ***
Home
    Has section      Welcome to InnoQIM
    Has section      Solutions   
    #Has section      Engagement                inconsistent format with other!
    Footer has       Address
    Footer has       Contact Info

Go Solutions
    Go section     Solutions        Functional Testing
    Go section     Solutions        Agile Testing
    #Go section     Solutions        System Integration Testing
    Go section     Solutions        Security Testing
    Go section     Solutions        Big Data Testing
    Go section     Solutions        Automation Testing
    Go section     Solutions        Performance Testing
    Go section     Solutions        Cloud Migration Testing

*** Variables ***
${BASE URL}     https://www.innoqim.com
${BROWSER}      Chrome

*** Keywords ***
Go to home page
    Open Browser                ${BASE URL}    ${BROWSER}
    Maximize Browser Window

Has section
    [Arguments]                     ${heading}
    ${count}                        Get Element Count       xpath://*[contains(text(),"${heading}")]/ancestor::*[@class="container"]
    Should Be Equal As Integers     ${count}                1                                                                               find ${heading}

Footer has
    [Arguments]      ${heading}
    ${count}                        Get Element Count       xpath://*[contains(text(),"${heading}")]/ancestor::*[@class="Footer"]
    Should Be Equal As Integers     ${count}                1                                                                               find ${heading}

Go section
    [Arguments]      ${caption}         ${item}
    Mouse Over       xpath://*[@class="MenuHeader"]//a[contains(text(),"${caption}")]/ancestor::li[contains(@class, "dropdown")]
    Click Link       ${item}
    Title should be  ${item}
