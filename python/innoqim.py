
from selenium import webdriver


def createWebDriver(browser='chrome'):
    """
    Create a webdriver
    browser = "chrome", using chromium
    browser = "firefox", using firefox
    """
    browser = browser.lower()
    assert browser in ['chrome', 'firefox']

    if browser == 'chrome':
        options = webdriver.ChromeOptions()
        options.add_argument('--ignore-certificate-errors')
        # options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(options=options)
    elif browser == 'firefox':
        # need FF 52, see https://github.com/SeleniumHQ/selenium/issues/3441
        capabilities = webdriver.DesiredCapabilities().FIREFOX
        capabilities['acceptInsecureCerts'] = True
        driver = webdriver.Firefox(capabilities=capabilities)

    return driver


def goHomePage(driver):
    driver.get("https://www.innoqim.com")
    driver.maximize_window()


def hasSection(driver, caption):
    xpath = '//*[contains(text(),"{}")]/ancestor::*[@class="container"]'.format(caption)
    elements = driver.find_elements_by_xpath(xpath)
    assert len(elements) == 1, "should find section: " + caption


def footerContains(driver, caption):
    xpath = '//*[contains(text(),"{}")]/ancestor::*[@class="Footer"]'.format(caption)
    elements = driver.find_elements_by_xpath(xpath)
    assert len(elements) == 1, "footer should contain: " + caption


def testHomePage(driver):
    for caption in [
        "Welcome to InnoQIM",
        "Solutions",
        # "Engagement"  # inconsistent format with other!
    ]:
        hasSection(driver, caption)

    for caption in ["Address", "Contact Info"]:
        footerContains(driver, caption)

    print("pass homepage test")


def goSection(driver, caption, menu):
    section = driver.find_element_by_xpath(
        '//*[@class="MenuHeader"]//a[contains(text(),"{}")]/ancestor::li[contains(@class, "dropdown")]'.format(caption))
    menu_link = driver.find_element_by_partial_link_text(menu)
    webdriver.ActionChains(driver).move_to_element(
        section).click(menu_link).perform()
    assert driver.title, menu


def testSolutionsSection(driver):
    for menu in [
        'Functional Testing',
        'Agile Testing',
        # 'System Integration Testing', // typo in web page
        'Security Testing',
        'Big Data Testing',
        'Automation Testing',
        'Performance Testing',
        'Cloud Migration Testing'
    ]:
        goHomePage(driver)
        goSection(driver, "Solutions", menu)

    print("pass solutions section test")


def runTest(driver):
    testHomePage(driver)
    testSolutionsSection(driver)


def main():
    driver = createWebDriver()

    try:
        goHomePage(driver)
        runTest(driver)
        print("pass all tests")
    finally:
        driver.save_screenshot("screenshot.png")
        driver.quit()


if __name__ == "__main__":
    main()
